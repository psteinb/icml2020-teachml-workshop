## History

To the best of our knowledge, the past ICML and NeurIPS conferences (years 2017-2019) have not hosted workshops focussing on methods for teaching Machine Learning.
